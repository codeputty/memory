﻿namespace Memory.UI
{
    public class GlobalState
    {
        public static GlobalState Instance { get; set; }

        public ThingRepository<MemorizedThing> ThingRepo { get; set; }
    }
}
