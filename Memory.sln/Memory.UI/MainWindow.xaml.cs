﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Memory.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private QuestionPicker questionPicker;

        private int ndx = 0;

        private List<MemorizedThing> _currentQuestions;

        public MemorizedThing CurrentThing
        {
            get
            {
                return _currentQuestions[ndx];
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            this.questionPicker = new QuestionPicker(GlobalState.Instance.ThingRepo);
            _currentQuestions = this.questionPicker.NextItems().ToList();

            this.DataContext = CurrentThing;
        }

        public void Next()
        {
            if (ndx == _currentQuestions.Count - 1)
            {
                _currentQuestions = questionPicker.NextItems().ToList();
                ndx = 0;
            }
            else
            {
                ndx++;
            }

            this.DataContext = CurrentThing;
        }

        private void Wrong_Click(object sender, RoutedEventArgs e)
        {
            CurrentThing.Wrong();
            this.Next();
        }

        private void Right_Click(object sender, RoutedEventArgs e)
        {
            CurrentThing.Right();
            this.Next();

        }
    }
}
