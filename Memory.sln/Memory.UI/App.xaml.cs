﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Memory.UI
{
    using System.IO;

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var globalState = new GlobalState();
            GlobalState.Instance = globalState;
            if (!File.Exists("data.json"))
            {
                File.Create("data.json");
            }
            globalState.ThingRepo = new ThingRepository<MemorizedThing>(@"data.json");
            
            if (!globalState.ThingRepo.GetAll().Any())
            {
                this.InitData();
            }
            globalState.ThingRepo.Persist();
        }

        private void InitData()
        {
            var data = File.ReadAllText(@"data\verbs.txt");
            
            var globalState = GlobalState.Instance;

            foreach (var line in data.Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries))
            {
                var parts = line.Split('\t');
                var question = parts[0];
                var answer = parts[1];
                globalState.ThingRepo.Add(new MemorizedThing() { Data = new Thing() { Question = question, Answer = answer } });
            }

        }

        protected override void OnExit(ExitEventArgs e)
        {
            GlobalState.Instance.ThingRepo.Persist();
            base.OnExit(e);
        }
    }
}
