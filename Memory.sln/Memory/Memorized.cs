﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memory
{
    public class Constants
    {
        public const int ResultHistory = 25;
    }

    public abstract class BaseIId : IId
    {
        public Guid Id { get; set; }

        protected BaseIId()
        {
            Id = Guid.NewGuid();
        }
    }

    public class MemoryItem<T> : BaseIId
    {
        public T Data { get; set; }

        [JsonIgnore]
        public FixedSizedQueue<bool> Results { get; private set; }

        [JsonIgnore]
        public Percentage Percentage { get; private set; }
        
        [JsonProperty("TheResults")]
        public bool[] Results2
        {
            get
            {
                return this.Results.Items.ToArray();
            }
            set
            {
                this.Results = new FixedSizedQueue<bool>(Constants.ResultHistory, value);
                this.ResetPercentage();
            }
        }

        public MemoryItem()
        {
            this.Results = new FixedSizedQueue<bool>(Constants.ResultHistory);
            this.Percentage = new Percentage();
            this.ResetPercentage();
        }

        public void Right()
        {
            this.Results.Add(true);
            this.ResetPercentage();
        }

        public void Wrong()
        {
            this.Results.Add(false);
            this.ResetPercentage();
        }

        private void ResetPercentage()
        {
            this.Percentage.NumRight = this.Results2.Count(x => x);
            this.Percentage.NumWrong = this.Results2.Count(x => !x);
        }
    }

    public class Percentage
    {
        public int NumRight { get; set; }

        public int NumWrong { get; set; }

        public int Total
        {
            get
            {
                return NumRight + NumWrong;
            }
        }

        public bool Is100Pct
        {
            get
            {
                return NumRight == NumWrong && NumRight != 0;
            }
        }

        public double Percent
        {
            get
            {
                if (Total == 0)
                {
                    return 0;
                }

                return ((double)NumRight) / Total;
            }
        }
    }

    public class MemorizedThing : MemoryItem<Thing>
    {
    }
}