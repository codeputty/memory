﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memory
{
    public interface IId
    {
        Guid Id { get; }
    }

    public class ThingRepository<T> where T :IId
    {
        private string _dataFile;

        private List<T> _items;

        public ThingRepository(string dataFile)
        {
            _dataFile = dataFile;

            string input;
            if (!File.Exists(dataFile) || (input = File.ReadAllText(dataFile)).Length == 0)
            {
                _items = new List<T>();
            }
            else
            {
                _items = JsonConvert.DeserializeObject<List<T>>(input);
            }
        }

        public T GetById(Guid id)
        {
            return _items.FirstOrDefault(x => x.Id.Equals(id));
        }

        public IQueryable<T> GetAll()
        {
            return _items.AsQueryable();
        }

        public void Persist()
        {
            File.WriteAllText(_dataFile,JsonConvert.SerializeObject(_items));
        }

        public void Add(T item)
        {
            _items.Add(item);
        }
    }
}
