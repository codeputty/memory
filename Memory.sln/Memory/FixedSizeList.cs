﻿namespace Memory
{
    using System.Collections.Concurrent;
    using System.Collections.Generic;

    public class FixedSizedQueue<T>
    {
        private ConcurrentQueue<T> q = new ConcurrentQueue<T>();

        public FixedSizedQueue(int limit, IEnumerable<T> items )
        {
            this.Limit = limit;
            foreach (var item in items)
            {
                this.Add(item);
            }
        }

        public FixedSizedQueue(int limit)
        {
            this.Limit = limit;
        }

        public int Limit { get; private set; }

        public void Add(T obj)
        {
            q.Enqueue(obj);
            lock (this)
            {
                T overflow;
                while (q.Count > Limit && q.TryDequeue(out overflow)) ;
            }
        }

        public IEnumerable<T> Items
        {
            get
            {
                return this.q;
            }
        }
    }
}
