﻿using System;
using System.Linq;

namespace Memory
{
    public class QuestionPicker
    {
        private ThingRepository<MemorizedThing> repo;

        private const int NumGood = 3;

        private const int NumToGet = 25;

        public QuestionPicker(ThingRepository<MemorizedThing> repo)
        {
            this.repo = repo;
        }

        public MemorizedThing[] NextItems()
        {
            var all = repo.GetAll().ToList();
            var good = all.Where(x => x.Percentage.Is100Pct).OrderBy(x=>Guid.NewGuid()).Take(NumGood).ToList();

            var toGet = NumToGet - good.Count;
            return
                good.Concat(
                    all.Where(x => !x.Percentage.Is100Pct)
                       .OrderByDescending(x => x.Percentage.Percent)
                       .Take(toGet)
                       .OrderBy(x => Guid.NewGuid())
                    )
                    .ToArray();
        }
    }
}
